# Chat Server Project

## Summary 
Create a chat server that accepts multiple clients and sends chat messages to all connected clients.
Create a log of the chat messages in a file on the server side. Users must log into the server to send messages. 
Users have to be already registered on the server to log in. (Don't need to implement this, just hard code for now)

Requirements: | |
--------------------|------------------------
Server              | Written in C
Client              | Written in Python
Compiled flags:     | `-std=c11 -Wall -Werror -Wvla -Wextra`<br>`-Wpedantic -Wwrite-strings -Wstack-usage=512`
Operating system:   | ubuntu 20.04
Innvocation:        | `./server <ip> <port>`
|                   | `./client <ip> <port>`
Features:           | Users will log into the server
|                   | All messages must be sent to all clients
|                   | Be able to ctl-c out of both client and server
No memory leaks:    | Use valgrind
Style:              | Barr C Style
Readability:        | Make modules and use SOLID principles
Log Format for Msgs:| `hh:mm:ss-username: msg` on screen and in log
Resources:          | [Beej's Guide](https://beej.us/guide/bgnet/html/index-wide.html)
|                   | [BSD Sockets](https://cis.temple.edu/~ingargio/old/cis307s96/readings/docs/sockets.html)
---------------------------------------------

## Protocols

Login Protocol: <br>
![Login](LoginProto.png) <br>
*Clients will use token to send messages.*

Sending Messages: <br>
![Messages](MsgProto.png)


## Msg Types

```
typedef enum pack_t_def {
    USER = 0x01,
    PASS = 0x02,
    MSG  = 0x03,
    ERR  = 0xFF,
} pack_t;
```